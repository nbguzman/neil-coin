pragma solidity ^0.4.16;

contract owned {
  address public owner;

  function owned()  public {
    owner = msg.sender;
  }

  modifier onlyOwner {
    require(msg.sender == owner);
    _;
  }
}


contract Economy is owned {
  uint baselineScore = 100;
  Vote[] public votes;
  Member[] public members;
  mapping (address => uint) public memberId;
  mapping (address => uint256) public balanceOf;
  mapping (string => uint) memberUIDToIdx;
  mapping (uint => Vote[]) votesReceived;
  mapping (uint => Vote[]) votesGiven;

  event Voted(address voter, address recipient, bool position, uint voteTime, uint recipientScore);
  event NewMemberAdded(address newMember, string uid, string name, uint since, uint score, string location);
  event WealthDistributed(address recipient, uint score, uint256 balance);

  struct Member {
    address member;
    string name;
    uint memberSince;
    uint age;
    string sex;
    string imageURL;
    uint score;
    string location;
  }

  struct Vote {
    address recipient;
    address sender;
    bool position;
    uint voteDate;
  }

  // Modifier that allows only shareholders to vote and create new proposals
  modifier onlyMembers {
    require(memberId[msg.sender] != 0);
    _;
  }

  /**
  * Constructor function
  */
  function Economy (uint256 initialSupply) public {
    // It’s necessary to add an empty first member
    /* addMember(0, "0", "empty first member", "", "", 0, 0, ""); */
    // and let's add the founder, to save a step later
    addMember(owner, "00000", "founder", "AI", "/images/founder.jpg", 0, "Alpha/Omega");
    balanceOf[owner] = initialSupply;
  }

  function getNumMembers() public constant returns (uint) {
    return members.length;
  }

  function addMember(address targetMember, string _uid, string memberName, string _sex, string url, uint _age, string _location)  public {
    uint id = memberId[targetMember];
    if (id == 0) {
      memberId[targetMember] = members.length;
      id = members.length++;
    }

    memberUIDToIdx[_uid] = id;

    members[id] = Member({
      member: targetMember,
      memberSince: now,
      name: memberName,
      sex: _sex,
      imageURL: url,
      age: _age,
      score: baselineScore,
      location: _location
    });
    NewMemberAdded(targetMember, _uid, memberName, now, baselineScore, _location);
  }

  function _getMemberProfile(uint idx) public constant returns(address, string , uint, uint, string, string, string) {
    return (
      members[idx].member,
      members[idx].name,
      members[idx].score,
      members[idx].age,
      members[idx].sex,
      members[idx].imageURL,
      members[idx].location
    );
  }

  function getMemberProfile(string _uid) public constant returns(address, string , uint, uint, string, string, string) {
    uint idx = memberUIDToIdx[_uid];
    return _getMemberProfile(idx);
  }

  function _getMemberScore(uint idx) public constant returns(address, string, uint) {
    return (
      members[idx].member,
      members[idx].name,
      members[idx].score
    );
  }

  function getMemberScore(string _uid) public constant returns(address, string, uint) {
    uint idx = memberUIDToIdx[_uid];
    return _getMemberScore(idx);
  }

  function _vote(
   bool _position,
   address _recipient
 )
   public
   returns (uint voteID)
 {
   uint recipientId = memberId[_recipient];
   uint senderId = memberId[msg.sender];

   Member storage mreceiver = members[recipientId];
   votesGiven[recipientId].push(Vote({
     recipient: _recipient,
     sender: msg.sender,
     position: _position,
     voteDate: now
   }));
   votesReceived[senderId].push(Vote({
     recipient: _recipient,
     sender: msg.sender,
     position: _position,
     voteDate: now
   }));

   if (_position) {
     mreceiver.score++;
   } else {
     mreceiver.score--;
   }

   // Create a log of this event
   Voted(msg.sender, _recipient, _position, now, mreceiver.score);
   return mreceiver.score;
 }

 function vote(
   bool _position,
   string _recipientUID
 )
   onlyMembers public
   returns (uint voteID)
 {
   uint idx = memberUIDToIdx[_recipientUID];
   return _vote(_position, members[idx].member);
 }

  function _transfer(address _recipient, uint256 _value) public {
    require(balanceOf[msg.sender] >= _value);
    require(balanceOf[_recipient] + _value >= balanceOf[_recipient]);
    balanceOf[msg.sender] -= _value;
    balanceOf[_recipient] += _value;
  }

  function transfer(string _uid, uint256 _value) public {
    uint idx = memberUIDToIdx[_uid];
    return _transfer(members[idx].member, _value);
  }

  function distributeWealth() payable public {
    Member storage first = members[1];
    _transfer(first.member, 200);
    WealthDistributed(first.member, first.score, balanceOf[first.member]);
  }
}
