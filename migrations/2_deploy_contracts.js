var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var greeter = artifacts.require("greeter");
var mortal = artifacts.require("mortal");
var owned = artifacts.require("owned");
var Economy = artifacts.require("Economy")

module.exports = function(deployer, network, accounts) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(greeter, "hola");
  deployer.deploy(Economy, 10000000);
  deployer.deploy(owned);
  // deployer
  //   .then(() => {
  //       return Economy.deployed();
  //   })
  //   .then((instance) => {
  //     let uids = {
  //       1: {
  //         address: accounts[1],
  //         uid: '1239395113',
  //         name: 'Alex Craig',
  //         sex: 'False',
  //         url: 'http://123.1.1.1/',
  //         age: 201,
  //         score: 100,
  //         location: 'Roboland'
  //       },
  //       // 2: {
  //       //   address: accounts[2],
  //       //   uid: '99999999',
  //       //   name: 'Robo Stani',
  //       //   sex: 'True',
  //       //   url: 'http://123.1.1.1/',
  //       //   age: 13,
  //       //   score: 222,
  //       //   location: 'Roboland'
  //       // },
  //       // 3: {
  //       //   address: accounts[3],
  //       //   uid: '8888',
  //       //   name: 'Presh Lingam',
  //       //   sex: 'False',
  //       //   url: 'http://123.1.1.1/',
  //       //   age: 12,
  //       //   score: 212,
  //       //   location: 'Domo'
  //       // },
  //     }
  //     for (let [uid, data] of Object.entries(uids)) {
  //         instance.addMember(
  //           data.address,
  //           uid,
  //           data.name,
  //           data.sex,
  //           data.url,
  //           data.age,
  //           data.score,
  //           data.location,
  //           {
  //             from: accounts[0],
  //           }
  //         ).then((res) => {
  //           console.log("added address: ", data.address," & new member: ", data);
  //         });
  //     }
  //     console.log("done deploying economy, accounts = ", accounts);
  //   })
  deployer.deploy(mortal);
};
