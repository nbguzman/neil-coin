module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    ec2: {
      host: "107.23.87.210",
      port: 8545,
      gas: 6721975,
      network_id: 2223 // Match any network id
    },
    local: {
      host: "127.0.0.1",
      port: 7545,
      gas: 6721975,
      network_id: 5777 // Match any network id,
    }
  }
};
