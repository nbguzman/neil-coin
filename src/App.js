import React, { Component } from 'react'
import SimpleStorageContract from '../build/contracts/SimpleStorage.json'
import greeter from '../build/contracts/greeter.json'
import EconomyContract from '../build/contracts/Economy.json'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      storageValue: 0,
      web3: null,
      greeterInstance: null,
      economyInstance: null,
      accounts: null
    }
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      })

      // Instantiate contract once web3 provided.
      this.instantiateContract()
    })
    .catch((err) => {
      console.log(err);
      console.log('Error finding web3.')
    })
  }


      handleClick(e) {
        this.state.greeterInstance.test_event("test", {from: this.state.accounts[0]}).then((res) => {
          console.log("testing event :", res);
        })
      }
      handleAddMemberClick(e) {
            let uids = {
              1: {
                address: this.state.accounts[1],
                uid: '1239395113',
                name: 'Alex Craig',
                sex: 'Male',
                age: 24,
                url: '/images/craig.jpg',
                location: 'Colony Prime'
              },
              2: {
                address: this.state.accounts[2],
                uid: '99999999',
                name: 'Robo Stani',
                sex: 'Male',
                url: '/images/robo.jpg',
                age: 13,
                location: 'Colony Prime'
              },
              3: {
                address: this.state.accounts[3],
                uid: '8888',
                name: 'Presh Lingam',
                sex: 'Male',
                url: '/images/presh.jpg',
                age: 12,
                location: 'Colony Mons'
              },
              4: {
                address: this.state.accounts[4],
                uid: '4444',
                name: 'Taylor Swift',
                sex: 'Female',
                url: '/images/TaylorSwift.jpg',
                age: 28,
                location: 'Colony Prime'
              }
            }

        for (let [_, data] of Object.entries(uids)) {
        this.state.economyInstance.addMember(
          data.address,
          data.uid,
          data.name,
          data.sex,
          data.url,
          data.age,
          data.location,
          {
            from: this.state.accounts[0],
            gas: 4000000
          }).then((res) => {
          console.log("add member event :", res);
        })
        }
      }
      handleVoteClick(e) {
        let position = e.target.value === 'true' ? true : false;

        console.log(position,
          this.state.accounts[1],
          {
            from: this.state.accounts[2],
            gas: 4000000
          })

        this.state.economyInstance._vote(
          position,
          this.state.accounts[1],
          {
            from: this.state.accounts[2],
            gas: 4000000
          }).then((res) => {
          console.log("vote event :", res);
        })
      }
      handleGetMemberClick(e) {
        let stuffs = ['1239395113', '99999999', '8888', '4444']
        for (let stuff of stuffs) {
        this.state.economyInstance.getMemberScore(
          stuff
        ).then((res) => {
          console.log("getting member: ", res);
        })
        }
      }
      handleDistribute(e) {
        this.state.economyInstance.distributeWealth(
          {
            from: this.state.accounts[0],
            gas: 4000000
          }
        ).then((res) => {
          console.log("transfering: ", res);
        })
      }
  instantiateContract() {
    /*
    * SMART CONTRACT EXAMPLE
    *
    * Normally these functions would be called in the context of a
    * state management library, but for convenience I've placed them here.
    */

    const contract = require('truffle-contract')
    const simpleStorage = contract(SimpleStorageContract)
    const economyContract = contract(EconomyContract)
    const g = contract(greeter)

    g.setProvider(this.state.web3.currentProvider)
    simpleStorage.setProvider(this.state.web3.currentProvider)
    economyContract.setProvider(this.state.web3.currentProvider)

    // Declaring this for later so we can chain functions on SimpleStorage.
    var simpleStorageInstance

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      this.setState({
        accounts: accounts
      })

      economyContract.deployed().then((instance) => {
        this.setState({
          economyInstance: instance
        })
        // instance.Voted({}, (err, ev) => {
        //   console.log("voted event = ", ev);
        // })
        // instance.NewMemberAdded({}, (err, ev) => {
        //   console.log("member added event = ", ev);
        // })
        // instance.WealthDistributed({}, (err, ev) => {
        //   console.log("wealth distributed event = ", ev);
        // })
      })

      g.deployed().then((instance) => {
        this.setState({
          greeterInstance: instance,
        })
        // instance.Greeted({}, (err, ev) => {
        //   console.log("event emitted: ", ev);
        // })
        instance.greet({from: accounts[0]}).then((res) => {
          console.log(res);
        })
      })

      simpleStorage.deployed().then((instance) => {
        simpleStorageInstance = instance
        // Stores a given value, 5 by default.
        return simpleStorageInstance.set(accounts.length, {from: accounts[0]})
      }).then((result) => {
        // Get the value from the contract to prove it worked.
        return simpleStorageInstance.get.call(accounts[0])
      }).then((result) => {
        // Update state with the result.
        return this.setState({ storageValue: result.c[0] })
      })


    })
  }

  render() {
    return (
      <div className="App">
      <nav className="navbar pure-menu pure-menu-horizontal">
      <a href="#" className="pure-menu-heading pure-menu-link">Truffle Box</a>
      </nav>

      <main className="container">
      <div className="pure-g">
      <div className="pure-u-1-1">
      <h1>Good to Go!</h1>
      <p>Your Truffle Box is installed and ready.</p>
      <h2>Smart Contract Example</h2>
      <p>If your contracts compiled and migrated successfully, below will show a stored value of 5 (by default).</p>
      <p>Try changing the value stored on <strong>line 59</strong> of App.js.</p>
      <p>The stored value is: {this.state.storageValue}</p>
      <button onClick={this.handleClick.bind(this)}>Emit Greet Event</button>
      <button onClick={this.handleAddMemberClick.bind(this)}>Add Member</button>
      <button onClick={this.handleVoteClick.bind(this)} value={true}>UpVote</button>
      <button onClick={this.handleVoteClick.bind(this)} value={false}>DownVote</button>
      <button onClick={this.handleGetMemberClick.bind(this)}>Get Member 1</button>
      <button onClick={this.handleDistribute.bind(this)}>Distribute</button>
      </div>
      </div>
      </main>
      </div>
    );
  }
}

export default App
